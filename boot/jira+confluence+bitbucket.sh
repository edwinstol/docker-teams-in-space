#!/bin/bash
echo "startup for jira+confluence+bitbucket.sh file"
source /opt/atlassian/setup.sh && tis-install
cd /opt/atlassian/scripts
./movetopresent.sh
cd /opt/atlassian
start-crowd && start-jira && start-confluence && start-bitbucket
sleep 30
tis-reindex
echo "successfully completed startup for Jira, Confluence, and Bitbucket"
tis-check && /bin/bash